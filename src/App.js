import "./App.css";
import NavbarCom from "./components/navbar";
// import Intro from "./components/intro";
// import Event from "./components/Event";
import Tampilan from "./page/tampilan";

import "./App.css";
import Profilee from "./page/Profilee";
import { BrowserRouter, Route, Switch } from "react-router-dom/cjs/react-router-dom.min";
import DetailProduct from "./page/detail_product";

function App() {
  return (
    <>
      <NavbarCom />
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/" component={Tampilan} exact />
            <Route path="/data_diri" component={Profilee} exact />
            <Route path="/detail_product/:id_product" component={DetailProduct} exact />
          </Switch>
        </main>
      </BrowserRouter>
    </>
  );
}

export default App;
