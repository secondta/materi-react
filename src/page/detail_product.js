import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";

function DetailProduct() {
  const { id_product } = useParams();
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    const getData = () => {
      axios
        .get("http://localhost:3030/products/" + id_product)
        .then((data) => setPosts(data.data))
        .catch((error) => console.log(error));
    };
    getData();
  }, []);
  return (
    <div>
      <h3>
        <span>{posts.id}</span> {posts.title}
      </h3>
      <p>{posts.price}</p>
    </div>
  );
}

export default DetailProduct;
